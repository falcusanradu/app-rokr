package com.rokr.app.rokrapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RokrAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(RokrAppApplication.class, args);
    }

}
