import React from 'react';
// import MaterialTable from '../../../node_modules/material-table';
// import '../table/customTable.scss';

export default function MaterialTableDemo() {
  const [state, setState] = React.useState({
    columns: [
      { title: 'Key Results', field: 'results' },
      { title: 'Current value', field: 'currentValue' },
      { title: 'Progress', field: 'progress', type: 'numeric' },
      { title: 'RAG',field: 'rag'},
      { title: "Owner", field: "owner"},
      { title: "Timeline", field: "timeline" },
      { title: "Last check-in", field: "lastCheckIn"}
    ],
    data: [
      { results: 'Sell Insurance 1', currentValue : '100$', progress: "20%", rag: "Red" , owner: "Sergiu", timeline: new Date().toString(), lastCheckIn: new Date().toString() },
      { results: 'Sell Insurance 1', currentValue : '200$', progress: "10%", rag: "Green" , owner: "Sergiu", timeline: new Date().toString(), lastCheckIn: new Date().toString() },
    ]
  });

  return (
    <dv>TODO Radu: make me work: MaterialTable</dv>
  );
}

// <MaterialTable
    //   title="Objectives"
    //   columns={state.columns}
    //   data={state.data}
    //   options={{
    //     paging: false
    //   }}
    //   editable={{
    //     onRowAdd: newData =>
    //       new Promise(resolve => {
    //         setTimeout(() => {
    //           resolve();
    //           setState(prevState => {
    //             const data = [...prevState.data];
    //             data.push(newData);
    //             return { ...prevState, data };
    //           });
    //         }, 600);
    //       }),
    //     onRowUpdate: (newData, oldData) =>
    //       new Promise(resolve => {
    //         setTimeout(() => {
    //           resolve();
    //           if (oldData) {
    //             setState(prevState => {
    //               const data = [...prevState.data];
    //               data[data.indexOf(oldData)] = newData;
    //               return { ...prevState, data };
    //             });
    //           }
    //         }, 600);
    //       }),
    //     onRowDelete: oldData =>
    //       new Promise(resolve => {
    //         setTimeout(() => {
    //           resolve();
    //           setState(prevState => {
    //             const data = [...prevState.data];
    //             data.splice(data.indexOf(oldData), 1);
    //             return { ...prevState, data };
    //           });
    //         }, 600);
    //       }),
    //   }}
    // />