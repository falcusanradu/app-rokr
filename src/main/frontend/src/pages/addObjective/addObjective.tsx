import * as React from "react";
import styles from "./addObjective.module.scss";
import commonStyles from "../../common/common.module.scss";
import PageRedirectComponent from "../../components/pageRedirectComponent/pageRedirectComponent";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface IaddObjectiveProps {}
interface IaddObjectiveState {
  selectedDate: Date;
}

class AddObjective extends React.Component<
  IaddObjectiveProps,
  IaddObjectiveState
> {
  constructor(props: IaddObjectiveProps) {
    super(props);
    this.state = {
      selectedDate: new Date()
    };
  }

  public render(): JSX.Element {
    return (
      <div className={`${commonStyles.pageWrapper}`}>
        <PageRedirectComponent selected="left"></PageRedirectComponent>
        <label className={`${styles.addObjectiveHeader}`}>New Objective</label>
        <div>
          <div className={`${styles.propertyContainer}`}>
            <label className={`${styles.propertyLabel}`}>Name: </label>
            <input
              className={[styles.propertyInput, styles.inputContainer].join(
                " "
              )}
            ></input>
          </div>
          <div className={`${styles.propertyContainer}`}>
            <label className={`${styles.propertyLabel}`}>Note: </label>
            <input
              className={[styles.propertyInput, styles.inputContainer].join(
                " "
              )}
            ></input>
          </div>
          <div className={`${styles.propertyContainer}`}>
            <label className={`${styles.propertyLabel}`}>Owner: </label>
            <div className={`${styles.inputContainer}`}>
              <input
                className={[styles.propertyInput, styles.ownerInput].join(" ")}
              ></input>
              <DatePicker className={[styles.propertyInput, styles.ownerDatepicker].join(" ")}
                selected={this.state.selectedDate}
                onChange={(date: Date) => {
                  this.setState({ selectedDate: date });
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

}

export default AddObjective;
